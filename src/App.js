import './App.css';
import {Outlet, Link} from "react-router-dom";
import {Breadcrumb, Layout} from 'antd';
import {HomeOutlined, FileDoneOutlined, SmileOutlined} from "@ant-design/icons";

const {
    Content,
    Header,
    Footer
} = Layout;

function App() {
    return (
        <Layout>
            <Header style={{background: '#f5f5f5'}}>
                <Breadcrumb
                    items={[
                        {
                            title: <Link to="/"><HomeOutlined/> Home</Link>,
                            key: 'Home',
                        },
                        {
                            title: <Link to="/done"><FileDoneOutlined/> Done List</Link>,
                            key: 'DoneList',
                        },
                        {
                            title: <Link to="/help"><SmileOutlined/> Help</Link>,
                            key: 'Help',
                        },
                    ]}
                />
            </Header>
            <Content><Outlet/></Content>
            <Footer></Footer>
        </Layout>
    );
}

export default App;
