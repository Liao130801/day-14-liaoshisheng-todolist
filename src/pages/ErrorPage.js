import {useRouteError} from "react-router-dom";

export default function ErrorPage() {
    const error = useRouteError()
    return (
        <div>
            <h1>error</h1>
            {error.statusText || error.message}
        </div>
    )
}