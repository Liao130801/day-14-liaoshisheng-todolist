import api from "./api"

export const getTodoList = () => {
    return api.get('/todos')
}

export const updateTodo = (id, todo) => {
    return api.put(`/todos/${id}`, todo)
}


export const deleteTodo = (id) => {
    return api.delete(`/todos/${id}`)
}

export const addTodo = (todo) => {
    return api.post(`/todos`, todo)
}

export const getTodoDetail = (id) => {
    return api.get(`/todos/${id}`)
}