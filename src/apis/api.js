import axios from "axios";

const api = axios.create({
    baseURL: 'http://localhost:8081',
})

api.interceptors.response.use(
    response => response,
    error => {
        alert('timeout')
        return Promise.reject(error)
    }
)

export default api