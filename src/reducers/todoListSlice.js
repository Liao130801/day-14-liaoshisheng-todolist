import {createSlice} from "@reduxjs/toolkit";

export const todoListSlice = createSlice({
    name: 'todolist',
    initialState: {
        todoList: []
    },
    reducers: {
        initToDoList: (state, action) => {
            state.todoList = action.payload
        }
    },
})

export default todoListSlice.reducer;
export const {initToDoList} = todoListSlice.actions;
