import './TodoList.css'
import {TodoGroup} from "./TodoGroup";
import {TodoGenerator} from "./TodoGenerator";
import {useEffect} from "react";
import {useTodos} from "../hooks/useTodo";

export const TodoList = () => {
    const { loadTodos } = useTodos()
    useEffect(() => {loadTodos()}, [loadTodos])
    return (
        <div className='todo-list-parent'>
            <h1 className='todo-list-title'>{"Todo List"}</h1>
            <TodoGenerator/>
            <TodoGroup/>
        </div>
    )
}
