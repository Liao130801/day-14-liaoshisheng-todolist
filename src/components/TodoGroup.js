import {TodoItem} from "./TodoItem";
import {useSelector} from "react-redux";

export const TodoGroup = () => {
    const todoList = useSelector(state => state.todolist.todoList);
    return (
        <div>
            {
                todoList?.map((todo) => {
                    return (
                        <TodoItem todo={todo} key={todo.id}/>
                    )
                })
            }
        </div>
    )
}
