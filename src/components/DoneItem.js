import "./TodoList.css"
import {Card, Modal, message, Row, Col} from "antd"
import {CloseOutlined} from "@ant-design/icons";
import {useNavigate} from "react-router-dom";
import {useTodos} from "../hooks/useTodo";
import {useState} from "react";


export const DoneItem = (props) => {
    const {todo} = props
    const [open, setOpen] = useState(false);
    const navigate = useNavigate()
    const {deleteTodo} = useTodos()
    const handleDelete = async () => {
        await deleteTodo(todo.id)
        success()
    }

    const handleNavigate = () => {
        navigate('/done/' + todo.id)
    }

    const success = () => {
        message.open({
            type: 'success',
            content: 'delete success',
        });
    };

    return (
        <div className="todo-item">
            <Card className={`todo-item-${todo.done ? 'del-' : ''}card`}>
                <Row justify="space-between">
                    <Col flex="auto" onClick={handleNavigate}>{todo.text}</Col>
                    <Col>
                        <CloseOutlined onClick={() => setOpen(true)}/>
                    </Col>
                </Row>
            </Card>
            <Modal
                open={open}
                onOk={handleDelete}
                onCancel={() => setOpen(false)}
                okText="确认"
                cancelText="取消"
            >
                是否要删除 {todo.text} ?
            </Modal>
        </div>
    )
}
