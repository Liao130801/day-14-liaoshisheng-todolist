import "./TodoList.css"
import {Card, Modal, message, Input} from "antd"
import {CloseOutlined, EditOutlined, EyeOutlined} from "@ant-design/icons";
import React, {useState} from "react";
import {useTodos} from "../hooks/useTodo";
import {Col, Row} from "antd";
import {useNavigate} from "react-router-dom";

export const TodoItem = (props) => {
    const navigate = useNavigate()
    const {todo} = props
    const [newText, setNewText] = useState('');
    const [modalVisible, setModalVisible] = useState(false);
    const [isEditing, setIsEditing] = useState(false);
    const {updateTodo, deleteTodo} = useTodos()

    const showModal = () => {
        setModalVisible(true);
    }
    const hideModal = () => {
        setModalVisible(false);
        setIsEditing(false);
    }

    const handleDelete = async () => {
        await deleteTodo(todo.id)
        success()
    }

    const handleUpdateDoneStatus = async () => {
        await updateTodo(todo.id, {text: todo.text, done: !todo.done});
    }

    const showEditModal = () => {
        setIsEditing(true);
        showModal();
    }

    const handleInputChange = (e) => {
        setNewText(e.target.value)
    }

    const handleModify = async () => {
        await updateTodo(todo.id, {text: newText, done: todo.done})
    }

    const handleOk = () => {
        isEditing ? handleModify() : handleDelete()
        success()
        hideModal()
    }

    const success = () => {
        message.open({
            type: 'success',
            content: isEditing ? 'edit' : 'delete' + ' success',
        });
    };
    const showDetail = () => {
        navigate('/done/' + todo.id)
    }

    return (
        <div className="todo-item">
            <Card className={`todo-item-${todo.done ? 'del-' : ''}card`}>
                <Row justify="space-between">
                    <Col flex="auto" onClick={handleUpdateDoneStatus}>{todo.text}</Col>
                    <Col>
                        <span className='close-icon'>
                            <CloseOutlined onClick={showModal}/>
                        </span>
                        <span className='edit-icon'>
                            <EditOutlined onClick={showEditModal}/>
                        </span>
                        <span className='detail-icon'>
                        <EyeOutlined onClick={showDetail}/>
                        </span>
                    </Col>
                </Row>

            </Card>

            <Modal
                open={modalVisible}
                onOk={handleOk}
                onCancel={hideModal}
            >
                {
                    isEditing ? (
                        <div>
                            <div className="todo-item-edit-span">
                                正在编辑： {todo.text}
                            </div>
                            <Input
                                value={newText || todo.text}
                                onChange={handleInputChange}
                                onPressEnter={handleModify}
                            />
                        </div>
                    ) : (
                        <div>
                            是否要删除 {todo.text} ?
                        </div>
                    )
                }

            </Modal>
        </div>
    )
}
