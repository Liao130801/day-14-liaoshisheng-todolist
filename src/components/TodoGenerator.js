import {useState} from "react";
import {Button, Input} from "antd";
import {useTodos} from "../hooks/useTodo";

export const TodoGenerator = () => {
    const [text, setText] = useState('')
    const {addTodo} = useTodos()
    const handleInputChange = (e) => {
        setText(e.target.value)
    }
    const handleAdd = async () => {
        await addTodo({text: text, done: false})
        setText('')
    }
    return (
        <div className='todo-generator'>
            <span className='span-generator'/>
            <Input rootClassName='input-generator'
                   value={text}
                   onChange={handleInputChange}
                   onPressEnter={handleAdd}/>
            <span className='span-generator'/>
            <Button type={"primary"} onClick={handleAdd}>add</Button>
            <span className='span-generator'/>
        </div>
    )
}
