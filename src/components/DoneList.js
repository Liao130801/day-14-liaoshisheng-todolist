import {DoneGroup} from "./DoneGroup";

export const DoneList = () => {
    const title = "Done List"
    return (
        <div className='todo-list-parent'>
            <h1 className='todo-list-title'>{title}</h1>
            <DoneGroup/>
        </div>
    )
}
