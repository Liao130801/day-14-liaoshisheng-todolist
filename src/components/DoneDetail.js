import {useParams} from "react-router-dom";
import {useSelector} from "react-redux";
import {useTodos} from "../hooks/useTodo";
import {useEffect, useState} from "react";

export const DoneDetail = () => {
    const {id} = useParams();
    const {getTodoDetail} = useTodos()
    const [todo, setTodo] = useState([])
    useEffect(() => {
        async function temp() {
            let res = await getTodoDetail(id);
            setTodo(res);
        }

        temp();
    }, [])

    return (
        <div>
            <h1>Detail</h1>
            <div>ID: {JSON.stringify(todo?.id)}</div>
            <div>TEXT: {JSON.stringify(todo?.text)}</div>
        </div>
    )
}
