import { configureStore } from '@reduxjs/toolkit'
import todoListReducer from "../reducers/todoListSlice";

export default configureStore({
    reducer: {todolist: todoListReducer}
})