import * as todoApi from "../apis/todo";
import {initToDoList} from "../reducers/todoListSlice";
import {useDispatch} from "react-redux";

export const useTodos = () => {
    const dispatch = useDispatch()

    async function loadTodos() {
        const response = await todoApi.getTodoList()
        dispatch(initToDoList(response.data))
    }

    const updateTodo = async (id, todo) => {
        await todoApi.updateTodo(id, todo)
        await loadTodos()
    }

    const deleteTodo = async (id) => {
        await todoApi.deleteTodo(id)
        await loadTodos()
    }

    const addTodo = async (todo) => {
        await todoApi.addTodo(todo)
        await loadTodos();
    }

    const getTodoDetail = async (id) => {
        let res = await todoApi.getTodoDetail(id)
        return res.data
    }

    return {
        loadTodos,
        updateTodo,
        deleteTodo,
        addTodo,
        getTodoDetail
    }
}